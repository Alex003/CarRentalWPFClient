﻿using CarRentalWPFClient.CustomControls;
using CarRentalWPFClient.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CarRentalWPFClient
{
    public class ViewModelToControlConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType().Equals(typeof(LoginViewModel)))
            {
                LoginRegisterControl control = new LoginRegisterControl();
                control.DataContext = value;
                return control;
            } else if (value.GetType().Equals(typeof(ViewCarsViewModel))) {
                ViewCarsControl control = new ViewCarsControl();
                control.DataContext = value;
                return control;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
