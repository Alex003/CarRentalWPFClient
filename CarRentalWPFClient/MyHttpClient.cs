﻿using CarRentalWPFClient.Models;
using CarRentalWPFClient.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalWPFClient
{
    class MyHttpClient
    {
        private static readonly MyHttpClient instance = new MyHttpClient();
        private HttpClient httpclient;

        public static MyHttpClient Instance
        {
            get
            {
                return instance;
            }
        }

        protected MyHttpClient()
        {
            httpclient = new HttpClient();
            httpclient.BaseAddress = new Uri("http://localhost:50023/api/");
            httpclient.DefaultRequestHeaders.Accept.Clear();
            httpclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<string> register(string Email, string Password)
        {
            string jsonobject = JsonConvert.SerializeObject(new { Email = Email, Password = Password, ConfirmPassword = Password });
            StringContent content = new StringContent(jsonobject, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            try
            {
                response = await httpclient.PostAsync(httpclient.BaseAddress + "Account/register/", content);
            }
            catch
            {
                return "Server Connection Error";
            }

            var responseAsString = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                return "Erfolgreich Registriert";
            }
            return responseAsString;
        }

        public async Task<bool> login(string Email, string Password)
        {
            string actualContent = "grant_type=password&username=" + Email + "&password=" + Password;
            StringContent content = new StringContent(actualContent, Encoding.UTF8, "application/x-www-form-urlencoded");
            HttpResponseMessage response;
            try
            {
                response = await httpclient.PostAsync("http://localhost:50023/Token", content);
            }
            catch
            {
                return false;
            }

            string responseAsString = await response.Content.ReadAsStringAsync();

            var responseAsDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseAsString);

            if (response.IsSuccessStatusCode)
            {
                httpclient.DefaultRequestHeaders.Add("Authorization", "bearer " + responseAsDictionary["access_token"]);
                return true;
            }
            return false;
        }

        public async Task<List<CarModel>> getCars()
        {
            List<CarModel> cars = new List<CarModel>();

            HttpResponseMessage response = await httpclient.GetAsync(httpclient.BaseAddress + "cars/");
            if (response.IsSuccessStatusCode)
            {
                string jsonobject = await response.Content.ReadAsStringAsync();
                cars = JsonConvert.DeserializeObject<List<CarModel>>(jsonobject);
            }

            return cars;
        }

        public async Task<List<RentalCarModel>> getRentals()
        {
            List<RentalCarModel> rentals = new List<RentalCarModel>();
            HttpResponseMessage response = await httpclient.GetAsync(httpclient.BaseAddress + "cars/Rentals");
            if (response.IsSuccessStatusCode)
            {
                string jsonobject = await response.Content.ReadAsStringAsync();
                rentals = JsonConvert.DeserializeObject<List<RentalCarModel>>(jsonobject);
            }

            return rentals;
        }

        public async Task<bool> RentCar(RentalCarModel rental)
        {
            string jsonobject = JsonConvert.SerializeObject(
                new
                {
                    car = rental.car,
                    rentStart = rental.rentStart,
                    rentEnd = rental.rentEnd
                });
            StringContent content = new StringContent(jsonobject, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            try
            {
                response = await httpclient.PostAsync(httpclient.BaseAddress + "cars/Rent/", content);
            }
            catch
            {
                return false;
            }

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> ReturnCar(int id)
        {
            HttpResponseMessage response;
            StringContent content = new StringContent("", Encoding.UTF8, "application/json");
            try
            {
                response = await httpclient.PostAsync(httpclient.BaseAddress + "cars/Return?id=" + id, content);
            }
            catch
            {
                return false;
            }
            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        //bei Fehler beim Registrieren wollte ich die Response parsen, aber das hat iwie nciht funktioniert.
        class HttpResponseHelper
        {
            [JsonProperty("Message")]
            public string message { get; set; }
            [JsonProperty("ModelState")]
            public ModelState modelState { get; set; }
        }
        class ModelState
        {
            public Dictionary<string, string> Errors { get; set; }
        }
    }
}
