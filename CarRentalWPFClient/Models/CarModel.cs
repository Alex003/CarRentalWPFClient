﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalWPFClient.Models
{
    public class CarModel
    {
        public int id;
        public string model;
        public double price;
    }
}
