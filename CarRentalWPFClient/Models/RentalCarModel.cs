﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalWPFClient.Models
{
    public class RentalCarModel
    {
        public CarModel car;
        public DateTime rentStart;
        public DateTime rentEnd;
    }
}
