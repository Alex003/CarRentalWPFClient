﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace CarRentalWPFClient.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public LoginViewModel(MainWindowViewModel parent) : base(parent)
        {
            loginCommand = new RelayCommand(login);
            registerCommand = new RelayCommand(register);
            usernameLogin = "Alex@asdf.at";
            usernameRegister = "";
        }

        private string usernameLogin;
        public string UsernameLogin
        {
            get
            {
                return usernameLogin;
            }
            set
            {
                if(!usernameLogin.Equals(value))
                {
                    usernameLogin = value;
                }
            }
        }
        private string usernameRegister;
        public string UsernameRegister
        {
            get
            {
                return usernameRegister;
            }
            set
            {
                if (!usernameRegister.Equals(value))
                {
                    usernameRegister = value;
                }
            }
        }

        public ICommand loginCommand { get; set; }
        public void login(object passwordBox)
        {
            PasswordBox pbox = passwordBox as PasswordBox;
            LoginAsync(pbox.Password, pbox);
        }

        private async Task LoginAsync(string password, PasswordBox passwordBox)
        {
            bool loginSuccess = await MyHttpClient.Instance.login(UsernameLogin, password);
            if (loginSuccess)
            {
                parent.SuperViewModel = new ViewCarsViewModel(parent);
            }
            else
            {
                passwordBox.Password = "";
                System.Windows.Forms.MessageBox.Show("Wrong Username or Password.");
            }
        }

        public ICommand registerCommand { get; set; }
        public void register(object passwordBox)
        {
            PasswordBox pbox = passwordBox as PasswordBox;
            RegisterAsync(pbox.Password);
        }

        private async Task RegisterAsync(string password)
        {
            string response = await MyHttpClient.Instance.register(UsernameRegister, password);
            System.Windows.Forms.MessageBox.Show(response);
        }
    }
}
