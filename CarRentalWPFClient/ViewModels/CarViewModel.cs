﻿using CarRentalWPFClient.Models;
using System;
using System.ComponentModel;

namespace CarRentalWPFClient.ViewModels
{
    public class CarViewModel
    {
        public CarViewModel(CarModel car)
        {
            Id = car.id;
            model = car.model;
            price = car.price;
            rentStart = DateTime.Today;
            rentEnd = DateTime.Today.AddDays(7);
        }
        public CarViewModel(RentalCarModel rentalcar)
        {
            Id = rentalcar.car.id;
            model = rentalcar.car.model;
            price = rentalcar.car.price;
            rentStart = rentalcar.rentStart;
            rentEnd = rentalcar.rentEnd;
        }

        public CarViewModel(CarViewModel car)
        {
            Id = car.Id;
            model = car.model;
            price = car.price;
            rentStart = car.rentStart;
            rentEnd = car.rentEnd;
        }

        public int Id { get; private set; }

        private string model;
        public string Model
        {
            get
            {
                return model;
            }
            set
            {
                if (model != value)
                {
                    model = value;
                }
            }
        }

        private double price;
        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                if(price != value)
                {
                    price = value;
                }
            }
        }

        private DateTime rentStart;
        public DateTime RentStart {
            get
            {
                return rentStart;
            }
            set
            {
                if(rentStart != value)
                {
                    rentStart = value;
                }
            }
        }

        private DateTime rentEnd;
        public DateTime RentEnd
        {
            get
            {
                return rentEnd;
            }
            set
            {
                if (rentEnd != value)
                {
                    rentEnd = value;
                }
            }
        }

        public string DisplayEndDate
        {
            get
            {
                return rentEnd.ToShortDateString();
            }
        }
    }
}
