﻿using CarRentalWPFClient.CustomControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalWPFClient.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
            SuperViewModel = new LoginViewModel(this);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private BaseViewModel superViewModel;
        public BaseViewModel SuperViewModel
        {
            get
            {
                return superViewModel;
            }

            set
            {
                if (superViewModel != value)
                {
                    superViewModel = value;
                    NotifyPropertyChanged("SuperViewModel");
                }
            }
        }
    }
}
