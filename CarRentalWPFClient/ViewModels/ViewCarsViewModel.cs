﻿using CarRentalWPFClient.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CarRentalWPFClient.ViewModels
{
    public class ViewCarsViewModel : BaseViewModel
    {
        public ICommand selectCarCommand { get; set; }
        public ICommand returnCarCommand { get; set; }
        public ICommand rentCarCommand { get; set; }

        public ViewCarsViewModel(MainWindowViewModel parent) : base(parent)
        {
            getCars();
            selectCarCommand = new RelayCommand(selectCar);
            returnCarCommand = new RelayCommand(returnCar);
            rentCarCommand = new RelayCommand(rentCar);
        }

        private ObservableCollection<CarViewModel> cars;
        public ObservableCollection<CarViewModel> Cars
        {
            get
            {
                return cars;
            }
            set
            {
                if(cars != value)
                {
                    cars = value;
                    NotifyPropertyChanged("Cars");
                }
            }
        }

        private CarViewModel selectedCar;
        public CarViewModel SelectedCar
        {
            get
            {
                return selectedCar;
            }
            set
            {
                if(selectedCar != value)
                {
                    selectedCar = value;
                    NotifyPropertyChanged("SelectedCar");
                }
            }
        }

        private ObservableCollection<CarViewModel> rentalCars;
        public ObservableCollection<CarViewModel> RentalCars
        {
            get
            {
                return rentalCars;
            }
            set
            {
                if (rentalCars != value)
                {
                    rentalCars = value;
                    NotifyPropertyChanged("RentalCars");
                }
            }
        }

        private async Task getCars()
        {
            Cars = new ObservableCollection<CarViewModel>((await MyHttpClient.Instance.getCars()).Select((car) => new CarViewModel(car)));
            RentalCars = new ObservableCollection<CarViewModel>((await MyHttpClient.Instance.getRentals()).Select((car) => new CarViewModel(car)));
            selectCar(Cars[0]);
            NotifyPropertyChanged("RentalCars");
            NotifyPropertyChanged("Cars");

        }

        public void selectCar(object car)
        {
            SelectedCar = new CarViewModel((car as CarViewModel));
            NotifyPropertyChanged("SelectedCar");
        }

        public async void rentCar(object obj)
        {
            CarViewModel car = obj as CarViewModel;
            if (await MyHttpClient.Instance.RentCar(new RentalCarModel
            {
                car = new CarModel() { id = car.Id, model = car.Model, price = car.Price },
                rentStart = car.RentStart,
                rentEnd = car.RentEnd
            }))
            {
                getCars();
            }
        }

        public async void returnCar(object obj)
        {
            CarViewModel car = obj as CarViewModel;
            if (await MyHttpClient.Instance.ReturnCar(car.Id))
            {
                getCars();
            }
        }
    }
}
